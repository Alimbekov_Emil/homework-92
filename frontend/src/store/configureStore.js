import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import userReducer, {initialState} from "./reducers/userReducer";
import chatReducer from "./reducers/chatReducer";
import axiosChat from "../axiosChat";


const rootReducer = combineReducers({
  users: userReducer,
  chat: chatReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
  saveToLocalStorage({
    users: {
      ...initialState,
      user: store.getState().users.user,
    },
  });
});

axiosChat.interceptors.request.use((config) => {
  try {
    config.headers["Authorization"] = store.getState().users.user.token;
  } catch (e) {
  }

  return config;
});

axiosChat.interceptors.response.use(
  (res) => res,
  (e) => {
    if (!e.response) {
      e.response = {data: {global: "No internet"}};
    }

    throw e;
  }
);

export default store;
