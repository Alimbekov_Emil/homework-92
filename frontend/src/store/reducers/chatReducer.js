const initialState = {
  messages: [],
  users: [],
};

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case "CONNECTED":
      return { ...state, messages: action.messages, users: action.users };
    case "NEW_USER":
      return { ...state, users: action.users };
    case "LEAVE_USER":
      return { ...state, users: action.users };
    case "NEW_MESSAGE":
      return {
        ...state,
        messages: [...state.messages, { user: action.user, text: action.text }],
      };
    default:
      return { ...state };
  }
};

export default chatReducer;
