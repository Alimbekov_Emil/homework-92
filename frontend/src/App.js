import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import AppToolbar from "./Component/UI/AppToolBar/AppToolBar";
import { Container, CssBaseline } from "@material-ui/core";
import Chat from "./Container/Chat/Chat";
import Login from "./Container/Login/Login";
import Register from "./Container/Register/Register";
import { useSelector } from "react-redux";

const ProtectedRoute = ({ isAllowed, redirectTo, ...props }) => {
  return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />;
};

const App = () => {
  const user = useSelector((state) => state.users.user);
  return (
    <>
      <CssBaseline />
      <header>
        <AppToolbar />
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <ProtectedRoute path="/" exact component={Chat} isAllowed={user} redirectTo="/login" />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
          </Switch>
        </Container>
      </main>
    </>
  );
};

export default App;
