const express = require("express");
const Message = require("../models/Message");
const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const messages = await Message.find();
    return res.send(messages);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.post("/", async (req, res) => {
  try {
    const message = new Message({
      text: req.body.text,
      user: req.body.user,
    });

    await message.save();
    return res.send(message);
  } catch (error) {
    return res.status(400).send(error);
  }
});

module.exports = router;
