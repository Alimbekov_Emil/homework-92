const express = require("express");
const cors = require("cors");
const users = require("./app/users");
const messages = require("./app/messages");
const mongoose = require("mongoose");
const exitHook = require("async-exit-hook");
const config = require("./config");
const Message = require("./models/Message");
const User = require("./models/User");

const app = express();

require("express-ws")(app);
app.use(express.json());
app.use(cors());

const port = 8000;

app.use("/messages", messages);
app.use("/users", users);

const activeConnections = {};

app.ws("/chat", async (ws, req) => {
  const token = req.query;

  if (!token) {
    return ws.send(JSON.stringify({ error: "No token present" }));
  }

  const user = await User.findOne(token);

  if (!user) {
    return ws.send(JSON.stringify({ error: "Wrong token!" }));
  }

  console.log("client connected name= " + user.username);

  activeConnections[user.displayName] = { ws, user };

  const users = Object.keys(activeConnections);

  const messagesFull = await Message.find().populate("user");
  const messages = messagesFull.slice(-30);

  if (activeConnections[user.displayName] !== undefined) {
    ws.send(JSON.stringify({ type: "CONNECTED", messages, users }));
  }

  Object.keys(activeConnections).forEach((key) => {
    const connection = activeConnections[key].ws;
    connection.send(
      JSON.stringify({
        type: "NEW_USER",
        users,
      })
    );
  });

  ws.on("message", async (msg) => {
    const decoded = JSON.parse(msg);

    if (decoded.type === "CREATE_MESSAGE") {
      const message = await new Message({
        text: decoded.message,
        user: decoded.user._id,
      });
      message.save();

      Object.keys(activeConnections).forEach((key) => {
        const connection = activeConnections[key].ws;
        connection.send(
          JSON.stringify({
            type: "NEW_MESSAGE",
            text: message.text,
            user: decoded.user,
          })
        );
      });
    }
  });

  ws.on("close", async () => {
    console.log("Client Disconnected! name=" + user.username);

    await delete activeConnections[user.displayName];

    if (activeConnections[user.displayName] === undefined) {
      const users = Object.keys(activeConnections);

      Object.keys(activeConnections).forEach((key) => {
        const connection = activeConnections[key].ws;
        connection.send(
          JSON.stringify({
            type: "LEAVE_USER",
            users,
          })
        );
      });
    }
  });
});

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(async (callback) => {
    await mongoose.disconnect();
    console.log("mongoose disconnected");
    callback();
  });
};

run().catch(console.error);
